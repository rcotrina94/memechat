var socket = io.connect(null, {rememberTransport: false, transports: ['WebSocket', 'Flash Socket', 'AJAX long-polling']});
var uname;
var c = 1;
var lastuser;
var audio = document.getElementById('blink');
audio.volume=0.2;

// on connection to server, ask for user's name with an anonymous callback
socket.on('connect', function() {
    // call the server-side function 'adduser' and send one parameter (value of prompt)
    socket.emit('adduser', uname = prompt("Ingresa un nombre de usuario"));
});

// listener, whenever the server emits 'updatechat', this updates the chat body
socket.on('updatechat', function(username, data) {
    var imagel = '<a class=\"pull-left\" href=\"#\"><img src=\"assets/holder.png\"></a>'
    var imager = '<a class=\"pull-right\" href=\"#\"><img src=\"assets/holder.png\"></a>'
    if (username === uname) {
        if (c !== 2) {
            $('#conversation li:last-child div.media-body').append('<p>' + data + '</p>');
        } else {
            $('#conversation').append('<li class=\"media nl prop\">'+imager+'<div class=\"media-body\"><h4>Yo:</h4><p>'+ data + '</p></div></li>');
            c++;
        }

    } else {
        if (lastuser === username && lastuser !== "SERVER") {
            $('#conversation li:last-child div.media-body').append('<p>' + data + '</p>');
            audio.load();
            audio.play();
        } else {
            
            if (username === "SERVER") {
                if (data === "Te has conectado"){
                    $('#conversation').append('<li class=\"alert alert-success\"><b>' + username + ':</b> ' + data + '<br></li>');
                } else {
                    $('#conversation').append('<li class=\"alert alert-neutral\"><b>' + username + ':</b> ' + data + '<br></li>');
                }
                
            } else {
                $('#conversation').append('<li class=\"media nl oth\">'+imagel+'<div class=\"media-body\"><h4>' + username + ':</h4><p>' + data + '</p></div></li>');
                audio.load();
                audio.play();
            }
        
        }
        c = 2;
    }
    lastuser = username;
    gobottom200();
});

// listener, whenever the server emits 'updateusers', this updates the username list
socket.on('updateusers', function(data) {
    $('#users').empty();
    $.each(data, function(key, value) {
        $('#users').append('<li>' + key + '</li>');
    });
    console.log(data);
});

function gobottom() {
    $("#chatbox").animate({scrollTop: $('#chatbox')[0].scrollHeight});
}

function gobottom200() {
    setTimeout(gobottom(), 500);
}

// on load of page
$(function() {


    // when the client clicks SEND
    $('#datasend').click(function() {
        if (document.getElementById('data').value !== "") {
            var message = $('#data').val();

            $('#data').val('');
            // tell server to execute 'sendchat' and send along one parameter
            socket.emit('sendchat', message);
            $('#data').focus();
        }
    });

    // when the client hits ENTER on their keyboard
    $('#data').keypress(function(e) {
        if (document.getElementById('data').value !== "") {
            if (e.which === 13) {
                $(this).blur();
                $('#datasend').focus().click();
                $('#data').focus();
            }
        }

    });
});